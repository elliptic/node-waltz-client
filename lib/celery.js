/* eslint-disable yoda */
const _ = require('lodash');
const E = require('http-errors');
const url = require('url');
const Promise = require('bluebird');
const celery = require('node-celery');
const uuidv4 = require('uuid/v4');

const createCeleryConfig = (config) => {
  const parsed_url = url.parse(config.celeryUrl, null, true);

  if (parsed_url == null) {
    throw new Error('Invalid celery url');
  }

  const {
    hostname, port, auth, pathname,
  } = parsed_url;

  if (
    hostname == null ||
    port == null ||
    auth == null ||
    pathname == null ||
    pathname.length === 0
  ) {
    throw new Error('Invalid celery url');
  }

  const [login, password] = auth.split(':');
  const vhost = pathname.slice(1);

  if (login == null || password == null || vhost == null) {
    throw new Error('Invalid celery url');
  }

  return {
    CELERY_RESULT_BACKEND: 'amqp',
    CELERY_TASK_RESULT_DURABLE: false,
    CELERY_BROKER_OPTIONS: {
      host: hostname,
      port,
      login,
      password,
      authMechanism: 'AMQPLAIN',
      vhost,
      heartbeat: 20,
      ssl: {
        enabled: true,
        rejectUnauthorized: true,
        caFile: config.ssl.caFile,
      },
    },
    DEFAULT_QUEUE: 'celery',
    CELERY_ROUTES: {
      source_of_funds: { queue: 'analyses' },
      source_of_funds_rpc: { queue: 'analyses' },
      address_cluster_neighbours: { queue: 'analyses' },
      address_cluster_neighbours_rpc: { queue: 'analyses' },
    },
  };
};

module.exports = class CeleryClient {
  constructor(config, log, contexty) {
    this.contexty = contexty || {};
    this.log = log;

    if (config.disabled) {
      this.log.warn('GraphServer celery client disabled');
      this.client = { call: () => {} };
    } else {
      const opts = createCeleryConfig(config);
      this.client = celery.createClient(opts);
      this.client.on('error', this.log.error);
    }

    this.getAddressClusterRpc = this.getAddressClusterRpc.bind(this);
    this.getClusterExposure = this.getClusterExposure.bind(this);
    this.getSourceOfFundsRpc = this.getSourceOfFundsRpc.bind(this);
    this.getTxExposureIntent = this.getTxExposureIntent.bind(this);
    this.submitAddressCluster = this.submitAddressCluster.bind(this);
    this.submitTxQueries = this.submitTxQueries.bind(this);
    this.getTxTraceRpc = this.getTxTraceRpc.bind(this);

    this._call = this._call.bind(this);
    this._fireAndForget = this._fireAndForget.bind(this);
  }

  _getRequestId() {
    if (!this.contexty.context || !this.contexty.context.requestId) {
      return uuidv4();
    }
    return this.contexty.context.requestId;
  }

  getAddressClusterRpc(base58) {
    return this._call('address_cluster_neighbours_rpc', [base58], {
      priority: 5,
    });
  }

  getClusterExposure(address) {
    return this._call('get_cluster_exposure', [address]);
  }

  getSourceOfFundsRpc(tx_info) {
    return this._call('source_of_funds_rpc', [tx_info], {
      priority: 5,
    });
  }

  getTxExposureIntent(txhash) {
    return this._call('get_tx_exposure_intent', [txhash]);
  }

  submitAddressCluster(analyses) {
    const queries = _.map(analyses, c => [c.id, c.reference]);
    return this._fireAndForget('address_cluster_neighbours', queries);
  }

  submitTxQueries(analyses) {
    const queries = _.map(analyses, c => [c.id, c.reference]);
    return this._fireAndForget('source_of_funds', queries);
  }

  getTxTraceRpc(hash, opts) {
    return this._call('tx_trace_rpc', [hash, opts]);
  }

  _fireAndForget(taskName, args_array, opts = {}) {
    this.client.call(taskName, [args_array], opts);
  }

  _call(taskName, args, opts = {}) {
    _.defaults(opts, { priority: 10 });
    const kwargs = { requestId: this._getRequestId() };

    const deferred = Promise.pending();
    const handler = createHandler(deferred);
    this.client.call(taskName, args, kwargs, opts, handler);

    return deferred.promise;
  }
};

const createHandler = deferred => (response) => {
  if (response.status === 'FAILURE') {
    const msg = response.result.exc_message || 'unknown error from waltz';
    return deferred.reject(new Error(msg));
  }

  if (response.status !== 'SUCCESS') {
    return deferred.reject(new Error('Unrecognised celery response'));
  }

  const payload = JSON.parse(response.result);

  // handle old response version
  if (payload == null || !payload._version) {
    return deferred.resolve(payload);
  }

  const { status_code } = payload;
  const body = JSON.parse(payload.body);

  if (200 <= status_code && status_code < 300) {
    return deferred.resolve(body);
  }

  const { message, code, description } = body;
  return deferred.reject(new E(status_code, message, { code, description }));
};
