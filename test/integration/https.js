const fs = require('fs');
const express = require('express');
const https = require('https');
const RestClient = require('../../lib/rest');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');

// Set up Chai and chai-as-promised
chai.should();
chai.use(chaiAsPromised);
const key = fs.readFileSync('test/files/ssl/server/server.key');
const cert = fs.readFileSync('test/files/ssl/server/server.crt');

describe('https support', () => {
  let server = null;
  before(() => {
    const https_options = {
      key,
      cert,
    };
    const PORT = 9090;
    const HOST = 'localhost';
    const app = express();
    app.get('/hello', (req, res) => {
      res.send('HEY!');
    });
    server = https.createServer(https_options, app).listen(PORT, HOST);
  });

  after(() => server.close());

  describe('rest client', () => {
    describe('without client key/cert', () => {
      it('should not connect', async () => {
        const client = new RestClient({ waltzUrls: ['https://localhost:9090'] });
        return client.call('GET', '/hello')
          .should.be.rejected;
      });
    });
    describe('with a valid CA and a client key/cert and hostname checking disabled', () => {
      it('should not connect', async () => {
        const client = new RestClient({
          waltzUrls: ['https://localhost:9090'],
          ssl: {
            clientKey: fs.readFileSync('test/files/ssl/client/client.key'),
            clientCert: fs.readFileSync('test/files/ssl/client/client.crt'),
            ca: fs.readFileSync('test/files/ssl/ca/ca.crt'),
            disableHostnameChecking: true,
          },
        });
        return client.call('GET', '/hello')
          .should.not.be.rejected;
      });
    });
    before(() => {


    });
  });
});
