const RestClient = require('../../lib/rest');
const E = require('http-errors');

describe('Waltz REST client', () => {
  describe('constructor', () => {
    it('should throw if no URLs specified', () => {
      expect(() => new RestClient({})).to.throw;
    });
    it('should throw if url list is not an array', () => {
      expect(() => new RestClient({ waltzUrls: true })).to.throw;
    });
  });

  const waltzUrls = [
    'http://foo',
  ];

  describe('_getRequestId', () => {
    describe('when initialized with contexty', () =>
      it('should pull id from context', () => {
        const rc = new RestClient({ waltzUrls }, {
          context: { requestId: '519ea7d6-a2f7-4b00-af62-0c9eb3b05850' },
        });
        rc._getRequestId().should.equal('519ea7d6-a2f7-4b00-af62-0c9eb3b05850');
      }));

    describe('when initialized without contexty', () =>
      it('should pull make random id', () => {
        const rc = new RestClient({ waltzUrls });
        const first = rc._getRequestId();
        const second = rc._getRequestId();
        first.should.to.be.a.uuid('v4');
        second.should.to.be.a.uuid('v4');
        first.should.not.equal(second);
      }));
  });

  describe('_getBlockHeight', () => {
    it('should fetch the blockheight', () => {
      const rc = new RestClient({ waltzUrls });
      rc._getBlockHeight({ headers: { 'x-ell-blockheight': 100 } })
        .should.equal(100);
    });
  });

  describe('_formatResponse', () => {
    describe('for a 200 response', () => {
      it('should return the body of the response', () => {
        const rc = new RestClient({ waltzUrls });
        rc._formatResponse({ statusCode: 200, body: 'foo' })
          .should.equal('foo');
      });
    });
    describe('for a 400 response', () => {
      it('should throw a 400 error', () => {
        const rc = new RestClient({ waltzUrls });
        const error = {
          message: 'foo', status: 400, code: 1234, description: 'blah',
        };
        expect(() => rc._formatResponse({ statusCode: 400, error }))
          .to.throw(E[400], 'foo')
          .which.has.all.keys('message', 'code', 'description');
      });
    });
    describe('for a 404 response', () => {
      it('should throw a 404 error', () => {
        const rc = new RestClient({ waltzUrls });
        const error = {
          message: 'foo', status: 404, code: 1234, description: 'blah',
        };
        expect(() => rc._formatResponse({ statusCode: 404, error }))
          .to.throw(E[404], 'foo')
          .which.has.all.keys('message', 'code', 'description');
      });
    });
  });

  describe('_pickResponse', () => {
    describe('when two requests are equivalent', () => {
      const r1 = { statusCode: 200, body: 'foo', headers: { 'X-Ell-Blockheight': 200 } };
      const r2 = { statusCode: 200, body: 'bar', headers: { 'X-Ell-Blockheight': 200 } };
      it('should return any of them', () => {
        const rc = new RestClient({ waltzUrls });
        ['foo', 'bar'].should.contain(rc._pickResponse(r1, r2).body);
      });
    });
    describe('when one request is newer', () => {
      const r1 = { statusCode: 200, body: 'foo', headers: { 'X-Ell-Blockheight': 100 } };
      const r2 = { statusCode: 200, body: 'bar', headers: { 'X-Ell-Blockheight': 200 } };
      it('should return the newer one', () => {
        const rc = new RestClient({ waltzUrls });
        rc._pickResponse(r1, r2).body.should.equal('bar');
      });
    });
    describe('when one request is failed', () => {
      const r1 = { statusCode: 200, body: 'foo', headers: { 'X-Ell-Blockheight': 100 } };
      const r2 = { statusCode: 500, body: 'bar', headers: { 'X-Ell-Blockheight': 200 } };
      it('should return successful one', () => {
        const rc = new RestClient({ waltzUrls });
        rc._pickResponse(r1, r2).body.should.equal('foo');
      });
    });
    describe('when both requests failed, one with 4xx and one with 4xx but one with higher blockheight', () => {
      const r1 = { statusCode: 400, body: 'foo', headers: { 'X-Ell-Blockheight': 100 } };
      const r2 = { statusCode: 404, body: 'bar', headers: { 'X-Ell-Blockheight': 200 } };
      it('should return the one with higher blockheight', () => {
        const rc = new RestClient({ waltzUrls });
        rc._pickResponse(r1, r2).body.should.equal('bar');
      });
    });
    describe('when both requests failed, one with 4xx and one with 5xx', () => {
      const r1 = { statusCode: 400, body: 'foo', headers: { 'X-Ell-Blockheight': 100 } };
      const r2 = { statusCode: 500, body: 'bar', headers: { 'X-Ell-Blockheight': 200 } };
      it('should return the 4xx', () => {
        const rc = new RestClient({ waltzUrls });
        rc._pickResponse(r1, r2).body.should.equal('foo');
      });
    });
    describe('when both requests failed with 500', () => {
      const r1 = { statusCode: 500, body: 'foo', headers: { 'X-Ell-Blockheight': 100 } };
      const r2 = { statusCode: 500, body: 'bar', headers: { 'X-Ell-Blockheight': 200 } };
      it('should throw', () => {
        const rc = new RestClient({ waltzUrls });
        expect(() => rc._pickResponse(r1, r2)).to.throw(E.InternalServerError);
      });
    });
  });
});
