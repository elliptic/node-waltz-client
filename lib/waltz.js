/* eslint-disable yoda */
const RestClient = require('./rest');
const CeleryClient = require('./celery');
const RabbotClient = require('./rabbot');

module.exports = class GraphServer {
  constructor(config, log, contexty, rabbit) {
    this.celeryClient = new CeleryClient(config.celery, log);
    this.restClient = new RestClient(config.restClient, contexty);
    if (rabbit) {
      this.rabbotClient = new RabbotClient(rabbit, this.restClient, config.rabbit);
    }

    // quicker, rest endpoints
    this.getAddress = this.restClient.getAddress;
    this.getAddressAutocomplete = this.restClient.getAddressAutocomplete;
    this.getAddressCluster = this.restClient.getAddressCluster;
    this.getAddresses = this.restClient.getAddresses;
    this.getAddressesTransactions = this.restClient.getAddressesTransactions;
    this.getAddressLookup = this.restClient.getAddressLookup;
    this.getAddressTransactions = this.restClient.getAddressTransactions;
    this.getClusterAddresses = this.restClient.getClusterAddresses;
    this.getClusterEdge = this.restClient.getClusterEdge;
    this.getClusterEdgeTransactions = this.restClient.getClusterEdgeTransactions;
    this.getClusterExplorerData = this.restClient.getClusterExplorerData;
    this.getClusterExplorerEdges = this.restClient.getClusterExplorerEdges;
    this.getClusterExplorerNeighbours = this.restClient.getClusterExplorerNeighbours;
    this.getClusterPiiBackward = this.restClient.getClusterPiiBackward;
    this.getClusterPiiForward = this.restClient.getClusterPiiForward;
    this.getClusterRiskyBackward = this.restClient.getClusterRiskyBackward;
    this.getClusterRiskyForward = this.restClient.getClusterRiskyForward;
    this.getClusters = this.restClient.getClusters;
    this.getClusterTransactions = this.restClient.getClusterTransactions;
    this.getEdgesBatch = this.restClient.getEdgesBatch;
    this.getLabelAutocomplete = this.restClient.getLabelAutocomplete;
    this.getStatus = this.restClient.getStatus;
    this.getTraceCoinsForward = this.restClient.getTraceCoinsForward;
    this.getTransactionLookup = this.restClient.getTransactionLookup;
    this.getTransactionsBatch = this.restClient.getTransactionsBatch;
    this.getTransactionsWithinDateRange = this.restClient.getTransactionsWithinDateRange;
    this.getTxDetail = this.restClient.getTxDetail;
    this.getTxhashTransactions = this.restClient.getTxhashTransactions;
    this.pingRpc = this.restClient.pingRpc;

    // possibly longer running queries
    // exposure
    this.getClusterExposure = this.celeryClient.getClusterExposure;
    this.getTxExposureIntent = this.celeryClient.getTxExposureIntent;

    // analysis queries
    this.getAddressClusterRpc = this.celeryClient.getAddressClusterRpc;
    this.getSourceOfFundsRpc = this.celeryClient.getSourceOfFundsRpc;

    this.submitAddressCluster = this.celeryClient.submitAddressCluster;
    this.submitTxQueries = this.celeryClient.submitTxQueries;

    // tx tracxing
    this.getTxTraceRpc = this.celeryClient.getTxTraceRpc;

    this.tearDown = this.tearDown.bind(this);
  }

  tearDown() {
    if (!this.rabbotClient) return undefined;
    return this.rabbotClient.stop();
  }
};
