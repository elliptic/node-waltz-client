# node-waltz-client

A simple client to interact with the waltz backend via amqp node-celery.

This has begun life as a simple rip-out of the existing 'GraphServer' from the
aml-api. It will obviously (hopefully) get better over time.


## Usage:

```javascript
const GraphServer = require('node-waltz-client');
const logger = { warn: console.log }
const client = new GraphServer({
  celery: { // options to pass to celery
    disabled: true // disable celery queue
    celeryUrl: // amqp url to the queue
    ssl: { // celery SSL options
      caFile: '/path/to/ca.crt' // CA file for the AMQP queue
    },
  },
  restClient: { // options to pass to the REST client
    waltzUrls: [
      'http://localhost:4567'
    ], // waltz URL or proxy to access
    ssl: {
      key: // optional, HTTP client key for authenticating HTTP requests to Waltz
      cert: //optional, HTTP client cert for authenaticating HTTP requests to Waltz
      ca: // optional, certificate authority (ca.crt)
      disableHostnameChecking: true, // optional, disable host name checking
    }
  }
}, logger)
```