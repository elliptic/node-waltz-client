const GraphServer = require('../../lib/waltz');

describe('Waltz client', () => {
  describe('calling rest endpoints', () => {
    const waltzClient = new GraphServer({
      celery: {
        disabled: true,
      },
      restClient: {
        waltzUrls: ['http://foo'],
      },
    }, { warn: () => null });

    const functions = [
      'getAddress',
      'getAddressAutocomplete',
      'getAddressCluster',
      'getAddresses',
      'getAddressesTransactions',
      'getAddressLookup',
      'getAddressTransactions',
      'getClusters',
      'getClusterAddresses',
      'getClusterEdge',
      'getClusterEdgeTransactions',
      'getClusterExplorerData',
      'getClusterExplorerEdges',
      'getClusterExplorerNeighbours',
      'getClusterPiiBackward',
      'getClusterPiiForward',
      'getClusterRiskyBackward',
      'getClusterRiskyForward',
      'getClusterTransactions',
      'getEdgesBatch',
      'getLabelAutocomplete',
      'getStatus',
      'getTraceCoinsForward',
      'getTransactionLookup',
      'getTransactionsBatch',
      'getTxDetail',
      'getTxhashTransactions',
      'pingRpc',
    ];
    functions.forEach((f) => {
      describe(`when calling ${f}`, () =>
        it('should bind to the rest client function', () => {
          waltzClient[f].should.equal(waltzClient.restClient[f]);
          waltzClient[f].should.not.equal(waltzClient.celeryClient[f]);
        }));
    });
  });
  describe('calling celery endpoints', () => {
    const waltzClient = new GraphServer({
      celery: {
        disabled: true,
      },
      restClient: {
        waltzUrls: ['http://foo'],
      },
    }, { warn: () => null });

    const functions = [
      'getClusterExposure',
      'getTxExposureIntent',
      'getAddressClusterRpc',
      'getSourceOfFundsRpc',
      'submitAddressCluster',
      'submitTxQueries',
      'getTxTraceRpc',
    ];
    functions.forEach((f) => {
      describe(`when calling ${f}`, () =>
        it('should bind to the rest client function', () => {
          waltzClient[f].should.not.equal(waltzClient.restClient[f]);
          waltzClient[f].should.equal(waltzClient.celeryClient[f]);
        }));
    });
  });
});
