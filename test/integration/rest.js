const shmock = require('shmock');
const RestClient = require('../../lib/rest');

const WALTZ_PORT_1 = 9000;
const WALTZ_PORT_2 = 9001;
const WALTZ_URL_1 = `http://localhost:${WALTZ_PORT_1}`;
const WALTZ_URL_2 = `http://localhost:${WALTZ_PORT_2}`;
const WALTZ_URLS = [WALTZ_URL_1, WALTZ_URL_2];

const E = require('http-errors');

describe('Waltz REST client', () => {
  let mock1 = null;
  let mock2 = null;

  const bootstrap = () => {
    mock1 = shmock(WALTZ_PORT_1);
    mock2 = shmock(WALTZ_PORT_2);
  };

  const teardown = () => {
    mock1.close();
    mock2.close();
  };

  before(bootstrap);
  after(teardown);

  describe('call', () => {
    describe('when there is only one server', () => {
      describe('and it is available', () => {
        const WALTZ_PATH = '/hello';
        const REQUEST_ID = '18ad3b03-0bb2-4796-8056-8186b2f2ab05';
        const contexty = { context: { requestId: REQUEST_ID } };
        let client = null;
        before(async () => {
          mock1
            .get(WALTZ_PATH)
            .set('X-Request-Id', REQUEST_ID)
            .reply(200, 'foo', { 'X-Ell-Blockheight': 100 });
        });

        before(() => {
          const config = {
            waltzUrls: [WALTZ_URL_1],
          };
          // the tests will fail if the x-request-id header is not attached
          client = new RestClient(config, contexty);
        });

        it('should reply with the server content', async () => {
          const res = await client.call('GET', '/hello', { foo: 'bar' });
          expect(res).not.to.be.null;
          res.should.deep.equal('foo');
          expect(client.clientOptions.body).to.be.undefined;
          return true;
        });
      });
      describe('and it returns a server error', () => {
        const WALTZ_PATH = '/hello';
        let client = null;
        before(async () => {
          mock1.get(WALTZ_PATH).reply(500, 'foo', { 'X-Ell-Blockheight': 100 });
        });

        before(() => {
          const config = {
            waltzUrls: [WALTZ_URL_1],
          };
          client = new RestClient(config);
        });

        it('should throw an internal server error', async () =>
          client
            .call('GET', WALTZ_PATH)
            .should.be.rejectedWith(E.InternalServerError, 'No Waltz servers are available.'));
      });
      describe('and it returns a 4xx error', () => {
        const WALTZ_PATH = '/hello';
        let client = null;
        before(async () => {
          mock1.get(WALTZ_PATH).reply(400, 'foo', { 'X-Ell-Blockheight': 100 });
        });

        before(() => {
          const config = {
            waltzUrls: [WALTZ_URL_1],
          };
          client = new RestClient(config);
        });

        it('should forward the error', async () =>
          client.call('GET', WALTZ_PATH).should.be.rejectedWith(E.BadRequest));
      });
      describe('and it replies too slowly', () => {
        const WALTZ_PATH = '/hello';
        let client = null;
        before(async () => {
          mock1
            .get(WALTZ_PATH)
            .delay(300)
            .reply(200, 'foo', { 'X-Ell-Blockheight': 100 });
        });

        before(() => {
          const config = {
            waltzUrls: [WALTZ_URL_1],
            timeoutMs: 200,
          };
          client = new RestClient(config);
        });

        it('should time out and return an error', async () =>
          client
            .call('GET', WALTZ_PATH)
            .should.be.rejectedWith(
              E.ServiceUnavailableError,
              'Timed out before any response from Waltz was received'
            ));
      });
      describe('and it is not available', () => {
        const WALTZ_PATH = '/hello';
        let client = null;
        before(() => {
          const config = {
            waltzUrls: ['http://foo'],
          };
          client = new RestClient(config);
        });

        it('should throw an internal server error', async () =>
          client
            .call('GET', WALTZ_PATH)
            .should.be.rejectedWith(E.InternalServerError, 'No Waltz servers are available.'));
      });
    });
    describe('when there are two servers available', () => {
      describe('when both servers are available', () => {
        const WALTZ_PATH = '/hello';
        let client = null;

        describe('when both servers return the same response', () => {
          before(async () => {
            mock1.get(WALTZ_PATH).reply(200, 'foo', { 'X-Ell-Blockheight': 100 });
            mock2.get(WALTZ_PATH).reply(200, 'foo', { 'X-Ell-Blockheight': 100 });
          });

          before(() => {
            const config = {
              waltzUrls: WALTZ_URLS,
            };
            client = new RestClient(config);
          });

          it('should reply with the server content', async () => {
            const res = await client.call('GET', '/hello');
            expect(res).not.to.be.null;
            res.should.deep.equal('foo');
            return true;
          });
        });
      });
      describe('when only one server is available', () => {
        const WALTZ_PATH = '/hello';
        let client = null;

        describe('and it returns something valid', () => {
          before(async () => {
            mock1.get(WALTZ_PATH).reply(200, 'foo', { 'X-Ell-Blockheight': 100 });
          });

          before(() => {
            const config = {
              waltzUrls: WALTZ_URLS,
            };
            client = new RestClient(config);
          });

          it('should reply with the working server content', async () => {
            const res = await client.call('GET', '/hello');
            expect(res).not.to.be.null;
            res.should.deep.equal('foo');
            return true;
          });
        });
        describe('and it returns 404', () => {
          before(() => {
            mock1.get(WALTZ_PATH).reply(404, { message: 'foo' });
            mock2.get(WALTZ_PATH).reply(500, { message: 'foo' });
          });

          before(() => {
            const config = {
              waltzUrls: WALTZ_URLS,
            };
            client = new RestClient(config);
          });

          it('should throw with the error', () =>
            client.call('GET', WALTZ_PATH).should.be.rejectedWith(E.NotFound, 'foo'));
        });
      });
      describe('when both servers are not available', () => {
        const WALTZ_PATH = '/hello';
        let client = null;

        describe('when both servers return 500', () => {
          before(async () => {
            mock1.get(WALTZ_PATH).reply(500, 'foo');
            mock2.get(WALTZ_PATH).reply(500, 'foo');
          });

          before(() => {
            const config = {
              waltzUrls: WALTZ_URLS,
            };
            client = new RestClient(config);
          });

          it('should reply with an internal server error', async () =>
            client
              .call('GET', WALTZ_PATH)
              .should.be.rejectedWith(E.InternalServerError, 'No Waltz servers are available.'));
        });
      });
    });
  });

  describe('getAddress', () => {
    const WALTZ_PATH = '/address/1';
    let client = null;

    describe('when one server has a newer response', () => {
      before(() => {
        mock1
          .get(WALTZ_PATH)
          .query('envelope=true')
          .reply(200, 'foo', { 'X-Ell-Blockheight': 200 });
        mock2
          .get(WALTZ_PATH)
          .query('envelope=true')
          .reply(200, 'bar', { 'X-Ell-Blockheight': 100 });
      });

      before(() => {
        const config = {
          waltzUrls: WALTZ_URLS,
        };
        client = new RestClient(config);
      });

      it('should reply with the server content', async () => {
        const res = await client.getAddress(1);
        expect(res).not.to.be.null;
        res.should.deep.equal('foo');
      });
    });
  });

  describe('getAddressAutocomplete', () => {
    const WALTZ_PATH = '/autocomplete/addresses/foo';
    let client = null;

    describe('when one server has a newer response', () => {
      before(() => {
        mock1
          .get(WALTZ_PATH)
          .query('envelope=true')
          .reply(200, 'foobar', { 'X-Ell-Blockheight': 200 });
        mock2
          .get(WALTZ_PATH)
          .query('envelope=true')
          .reply(200, 'foobaz', { 'X-Ell-Blockheight': 100 });
      });

      before(() => {
        const config = {
          waltzUrls: WALTZ_URLS,
        };
        client = new RestClient(config);
      });

      it('should reply with the server content', async () => {
        const res = await client.getAddressAutocomplete('foo');
        expect(res).not.to.be.null;
        res.should.deep.equal('foobar');
      });
    });
  });

  describe('getAddressCluster', () => {
    const WALTZ_PATH = '/address/1/cluster';
    let client = null;

    describe('when one server has a newer response', () => {
      before(() => {
        mock1
          .get(WALTZ_PATH)
          .query('envelope=true')
          .reply(200, 'foo', { 'X-Ell-Blockheight': 200 });
        mock2
          .get(WALTZ_PATH)
          .query('envelope=true')
          .reply(200, 'bar', { 'X-Ell-Blockheight': 100 });
      });

      before(() => {
        const config = {
          waltzUrls: WALTZ_URLS,
        };
        client = new RestClient(config);
      });

      it('should reply with the server content', async () => {
        const res = await client.getAddressCluster(1);
        expect(res).not.to.be.null;
        res.should.deep.equal('foo');
      });
    });
  });

  describe('getAddresses', () => {
    const WALTZ_PATH = '/addresses';
    let client = null;

    describe('when one server has a newer response', () => {
      before(() => {
        mock1
          .post(WALTZ_PATH)
          .query('envelope=true')
          .reply(200, 'foo', { 'X-Ell-Blockheight': 200 });
        mock2
          .post(WALTZ_PATH)
          .query('envelope=true')
          .reply(200, 'bar', { 'X-Ell-Blockheight': 100 });
      });

      before(() => {
        const config = {
          waltzUrls: WALTZ_URLS,
        };
        client = new RestClient(config);
      });

      it('should reply with the server content', async () => {
        const res = await client.getAddresses({ foo: 'bar' });
        expect(res).not.to.be.null;
        res.should.deep.equal('foo');
      });
    });
  });

  describe('getAddressesTransactions', () => {
    const WALTZ_PATH = '/txs';
    let client = null;

    describe('when one server has a newer response', () => {
      before(() => {
        mock1
          .post(WALTZ_PATH)
          .query('envelope=true')
          .reply(200, 'foo', { 'X-Ell-Blockheight': 200 });
        mock2
          .post(WALTZ_PATH)
          .query('envelope=true')
          .reply(200, 'bar', { 'X-Ell-Blockheight': 100 });
      });

      before(() => {
        const config = {
          waltzUrls: WALTZ_URLS,
        };
        client = new RestClient(config);
      });

      it('should reply with the server content', async () => {
        const res = await client.getAddressesTransactions({ fizz: 'buzz' });
        expect(res).not.to.be.null;
        res.should.deep.equal('foo');
      });
    });
  });

  describe('getAddressLookup', () => {
    const WALTZ_PATH = '/lookup/addresses';
    let client = null;

    describe('when one server has a newer response', () => {
      before(() => {
        mock1
          .post(WALTZ_PATH)
          .query('envelope=true')
          .reply(200, 'foo', { 'X-Ell-Blockheight': 200 });
        mock2
          .post(WALTZ_PATH)
          .query('envelope=true')
          .reply(200, 'bar', { 'X-Ell-Blockheight': 100 });
      });

      before(() => {
        const config = {
          waltzUrls: WALTZ_URLS,
        };
        client = new RestClient(config);
      });

      it('should reply with the server content', async () => {
        const res = await client.getAddressLookup('foo');
        expect(res).not.to.be.null;
        res.should.deep.equal('foo');
      });
    });
  });

  describe('getAddressTransactions', () => {
    const WALTZ_PATH = '/address/foo/txs';
    let client = null;

    describe('when one server has a newer response', () => {
      before(() => {
        mock1
          .post(WALTZ_PATH)
          .query('envelope=true')
          .send({ baz: 1 })
          .reply(200, 'foo', { 'X-Ell-Blockheight': 200 });
        mock2
          .post(WALTZ_PATH)
          .query('envelope=true')
          .send({ baz: 1 })
          .reply(200, 'bar', { 'X-Ell-Blockheight': 100 });
      });

      before(() => {
        const config = {
          waltzUrls: WALTZ_URLS,
        };
        client = new RestClient(config);
      });

      it('should reply with the server content', async () => {
        const res = await client.getAddressTransactions('foo', { baz: 1 });
        expect(res).not.to.be.null;
        res.should.deep.equal('foo');
      });
    });
  });

  describe('getClusterAddresses', () => {
    const WALTZ_PATH = '/cluster/foo/addresses';
    let client = null;

    describe('when one server has a newer response', () => {
      before(() => {
        mock1
          .post(WALTZ_PATH)
          .query('envelope=true')
          .send({ baz: 1 })
          .reply(200, 'foo', { 'X-Ell-Blockheight': 200 });
        mock2
          .post(WALTZ_PATH)
          .query('envelope=true')
          .send({ baz: 1 })
          .reply(200, 'bar', { 'X-Ell-Blockheight': 100 });
      });

      before(() => {
        const config = {
          waltzUrls: WALTZ_URLS,
        };
        client = new RestClient(config);
      });

      it('should reply with the server content', async () => {
        const res = await client.getClusterAddresses('foo', { baz: 1 });
        expect(res).not.to.be.null;
        res.should.deep.equal('foo');
      });
    });
  });

  describe('getClusterEdge', () => {
    const WALTZ_PATH = '/cluster-edge/foo/bar';
    let client = null;

    describe('when one server has a newer response', () => {
      before(() => {
        mock1
          .get(WALTZ_PATH)
          .query('envelope=true')
          .reply(200, 'foo', { 'X-Ell-Blockheight': 200 });
        mock2
          .get(WALTZ_PATH)
          .query('envelope=true')
          .reply(200, 'bar', { 'X-Ell-Blockheight': 100 });
      });

      before(() => {
        const config = {
          waltzUrls: WALTZ_URLS,
        };
        client = new RestClient(config);
      });

      it('should reply with the server content', async () => {
        const res = await client.getClusterEdge('foo', 'bar');
        expect(res).not.to.be.null;
        res.should.deep.equal('foo');
      });
    });
  });

  describe('getClusterEdgeTransactions', () => {
    const WALTZ_PATH = '/cluster-edge/foo/bar/txs';
    let client = null;

    describe('when one server has a newer response', () => {
      before(() => {
        mock1
          .post(WALTZ_PATH)
          .query('envelope=true')
          .send({ baz: 1 })
          .reply(200, 'foo', { 'X-Ell-Blockheight': 200 });
        mock2
          .post(WALTZ_PATH)
          .query('envelope=true')
          .send({ baz: 1 })
          .reply(200, 'bar', { 'X-Ell-Blockheight': 100 });
      });

      before(() => {
        const config = {
          waltzUrls: WALTZ_URLS,
        };
        client = new RestClient(config);
      });

      it('should reply with the server content', async () => {
        const res = await client.getClusterEdgeTransactions('foo', 'bar', { baz: 1 });
        expect(res).not.to.be.null;
        res.should.deep.equal('foo');
      });
    });
  });

  describe('getClusterExplorerData', () => {
    const WALTZ_PATH = '/explorer/basic';
    let client = null;

    describe('when one server has a newer response', () => {
      before(() => {
        mock1
          .post(WALTZ_PATH)
          .query('envelope=true')
          .send({ baz: 1 })
          .reply(200, 'foo', { 'X-Ell-Blockheight': 200 });
        mock2
          .post(WALTZ_PATH)
          .query('envelope=true')
          .send({ baz: 1 })
          .reply(200, 'bar', { 'X-Ell-Blockheight': 100 });
      });

      before(() => {
        const config = {
          waltzUrls: WALTZ_URLS,
        };
        client = new RestClient(config);
      });

      it('should reply with the server content', async () => {
        const res = await client.getClusterExplorerData({ baz: 1 });
        expect(res).not.to.be.null;
        res.should.deep.equal('foo');
      });
    });
  });

  describe('getClusterExplorerEdges', () => {
    const WALTZ_PATH = '/explorer/edges';
    let client = null;

    describe('when one server has a newer response', () => {
      before(() => {
        mock1
          .post(WALTZ_PATH)
          .query('envelope=true')
          .send({ baz: 1 })
          .reply(200, 'foo', { 'X-Ell-Blockheight': 200 });
        mock2
          .post(WALTZ_PATH)
          .query('envelope=true')
          .send({ baz: 1 })
          .reply(200, 'bar', { 'X-Ell-Blockheight': 100 });
      });

      before(() => {
        const config = {
          waltzUrls: WALTZ_URLS,
        };
        client = new RestClient(config);
      });

      it('should reply with the server content', async () => {
        const res = await client.getClusterExplorerEdges({ baz: 1 });
        expect(res).not.to.be.null;
        res.should.deep.equal('foo');
      });
    });
  });

  describe('getClusterExplorerNeighbours', () => {
    const WALTZ_PATH = '/explorer/clusters/foo/neighbours';
    let client = null;

    describe('when one server has a newer response', () => {
      before(() => {
        mock1
          .post(WALTZ_PATH)
          .query('envelope=true')
          .send({ baz: 1 })
          .reply(200, 'foo', { 'X-Ell-Blockheight': 200 });
        mock2
          .post(WALTZ_PATH)
          .query('envelope=true')
          .send({ baz: 1 })
          .reply(200, 'bar', { 'X-Ell-Blockheight': 100 });
      });

      before(() => {
        const config = {
          waltzUrls: WALTZ_URLS,
        };
        client = new RestClient(config);
      });

      it('should reply with the server content', async () => {
        const res = await client.getClusterExplorerNeighbours('foo', { baz: 1 });
        expect(res).not.to.be.null;
        res.should.deep.equal('foo');
      });
    });
  });

  describe('getClusterExposure', () => {
    const WALTZ_PATH = '/address/foo/exposureSource';
    let client = null;

    describe('when one server has a newer response', () => {
      before(() => {
        mock1
          .get(WALTZ_PATH)
          .query('envelope=true')
          .reply(200, 'foo', { 'X-Ell-Blockheight': 200 });
        mock2
          .get(WALTZ_PATH)
          .query('envelope=true')
          .reply(200, 'bar', { 'X-Ell-Blockheight': 100 });
      });

      before(() => {
        const config = {
          waltzUrls: WALTZ_URLS,
        };
        client = new RestClient(config);
      });

      it('should reply with the server content', async () => {
        const res = await client.getClusterExposure('foo');
        expect(res).not.to.be.null;
        res.should.deep.equal('foo');
      });
    });
  });

  describe('getClusterPiiBackward', () => {
    const WALTZ_PATH = '/explorer/pii_backward';
    let client = null;

    describe('when one server has a newer response', () => {
      before(() => {
        mock1
          .post(WALTZ_PATH)
          .query('envelope=true')
          .send({ baz: 1 })
          .reply(200, 'foo', { 'X-Ell-Blockheight': 200 });
        mock2
          .post(WALTZ_PATH)
          .query('envelope=true')
          .send({ baz: 1 })
          .reply(200, 'bar', { 'X-Ell-Blockheight': 100 });
      });

      before(() => {
        const config = {
          waltzUrls: WALTZ_URLS,
        };
        client = new RestClient(config);
      });

      it('should reply with the server content', async () => {
        const res = await client.getClusterPiiBackward({ baz: 1 });
        expect(res).not.to.be.null;
        res.should.deep.equal('foo');
      });
    });
  });

  describe('getClusterPiiForward', () => {
    const WALTZ_PATH = '/explorer/pii_forward';
    let client = null;

    describe('when one server has a newer response', () => {
      before(() => {
        mock1
          .post(WALTZ_PATH)
          .query('envelope=true')
          .send({ baz: 1 })
          .reply(200, 'foo', { 'X-Ell-Blockheight': 200 });
        mock2
          .post(WALTZ_PATH)
          .query('envelope=true')
          .send({ baz: 1 })
          .reply(200, 'bar', { 'X-Ell-Blockheight': 100 });
      });

      before(() => {
        const config = {
          waltzUrls: WALTZ_URLS,
        };
        client = new RestClient(config);
      });

      it('should reply with the server content', async () => {
        const res = await client.getClusterPiiForward({ baz: 1 });
        expect(res).not.to.be.null;
        res.should.deep.equal('foo');
      });
    });
  });

  describe('getClusterRiskyBackward', () => {
    const WALTZ_PATH = '/explorer/risky_backward';
    let client = null;

    describe('when one server has a newer response', () => {
      before(() => {
        mock1
          .post(WALTZ_PATH)
          .query('envelope=true')
          .send({ baz: 1 })
          .reply(200, 'foo', { 'X-Ell-Blockheight': 200 });
        mock2
          .post(WALTZ_PATH)
          .query('envelope=true')
          .send({ baz: 1 })
          .reply(200, 'bar', { 'X-Ell-Blockheight': 100 });
      });

      before(() => {
        const config = {
          waltzUrls: WALTZ_URLS,
        };
        client = new RestClient(config);
      });

      it('should reply with the server content', async () => {
        const res = await client.getClusterRiskyBackward({ baz: 1 });
        expect(res).not.to.be.null;
        res.should.deep.equal('foo');
      });
    });
  });

  describe('getClusterRiskyForward', () => {
    const WALTZ_PATH = '/explorer/risky_forward';
    let client = null;

    describe('when one server has a newer response', () => {
      before(() => {
        mock1
          .post(WALTZ_PATH)
          .query('envelope=true')
          .send({ baz: 1 })
          .reply(200, 'foo', { 'X-Ell-Blockheight': 200 });
        mock2
          .post(WALTZ_PATH)
          .query('envelope=true')
          .send({ baz: 1 })
          .reply(200, 'bar', { 'X-Ell-Blockheight': 100 });
      });

      before(() => {
        const config = {
          waltzUrls: WALTZ_URLS,
        };
        client = new RestClient(config);
      });

      it('should reply with the server content', async () => {
        const res = await client.getClusterRiskyForward({ baz: 1 });
        expect(res).not.to.be.null;
        res.should.deep.equal('foo');
      });
    });
  });

  describe('getClusters', () => {
    const WALTZ_PATH = '/clusters';
    let client = null;

    describe('when one server has a newer response', () => {
      before(() => {
        mock1
          .post(WALTZ_PATH)
          .query('envelope=true')
          .send({ baz: 1 })
          .reply(200, 'foobar', { 'X-Ell-Blockheight': 200 });
        mock2
          .post(WALTZ_PATH)
          .query('envelope=true')
          .send({ baz: 1 })
          .reply(200, 'foobaz', { 'X-Ell-Blockheight': 100 });
      });

      before(() => {
        const config = {
          waltzUrls: WALTZ_URLS,
        };
        client = new RestClient(config);
      });

      it('should reply with the server content', async () => {
        const res = await client.getClusters({ baz: 1 });
        expect(res).not.to.be.null;
        res.should.deep.equal('foobar');
      });
    });
  });

  describe('getClusterTransactions', () => {
    const WALTZ_PATH = '/cluster/foo/txs';
    let client = null;

    describe('when one server has a newer response', () => {
      before(() => {
        mock1
          .post(WALTZ_PATH)
          .query('envelope=true')
          .send({ baz: 1 })
          .reply(200, 'foo', { 'X-Ell-Blockheight': 200 });
        mock2
          .post(WALTZ_PATH)
          .query('envelope=true')
          .send({ baz: 1 })
          .reply(200, 'bar', { 'X-Ell-Blockheight': 100 });
      });

      before(() => {
        const config = {
          waltzUrls: WALTZ_URLS,
        };
        client = new RestClient(config);
      });

      it('should reply with the server content', async () => {
        const res = await client.getClusterTransactions('foo', { baz: 1 });
        expect(res).not.to.be.null;
        res.should.deep.equal('foo');
      });
    });
  });

  describe('getEdgesBatch', () => {
    const WALTZ_PATH = '/cluster-edges/batch';
    let client = null;

    describe('when one server has a newer response', () => {
      before(() => {
        mock1
          .post(WALTZ_PATH)
          .query('envelope=true')
          .send({ baz: 1 })
          .reply(200, 'foo', { 'X-Ell-Blockheight': 200 });
        mock2
          .post(WALTZ_PATH)
          .query('envelope=true')
          .send({ baz: 1 })
          .reply(200, 'bar', { 'X-Ell-Blockheight': 100 });
      });

      before(() => {
        const config = {
          waltzUrls: WALTZ_URLS,
        };
        client = new RestClient(config);
      });

      it('should reply with the server content', async () => {
        const res = await client.getEdgesBatch({ baz: 1 });
        expect(res).not.to.be.null;
        res.should.deep.equal('foo');
      });
    });
  });

  describe('getLabelAutocomplete', () => {
    const WALTZ_PATH = '/autocomplete/labels/foo';
    let client = null;

    describe('when one server has a newer response', () => {
      before(() => {
        mock1
          .get(WALTZ_PATH)
          .query('envelope=true')
          .reply(200, 'foobar', { 'X-Ell-Blockheight': 200 });
        mock2
          .get(WALTZ_PATH)
          .query('envelope=true')
          .reply(200, 'foobaz', { 'X-Ell-Blockheight': 100 });
      });

      before(() => {
        const config = {
          waltzUrls: WALTZ_URLS,
          retries: 0,
        };
        client = new RestClient(config);
      });

      it('should reply with the server content', async () => {
        const res = await client.getLabelAutocomplete('foo');
        expect(res).not.to.be.null;
        res.should.deep.equal('foobar');
      });
    });
  });

  describe('getStatus', () => {
    const WALTZ_PATH = '/status';
    let client = null;

    describe('when one server has a newer response', () => {
      before(() => {
        mock1
          .get(WALTZ_PATH)
          .query('') // so that it's not called with the envelope
          .reply(200, 'foo', { 'X-Ell-Blockheight': 200 });
        mock2
          .get(WALTZ_PATH)
          .query('')
          .reply(200, 'bar', { 'X-Ell-Blockheight': 100 });
      });

      before(() => {
        const config = {
          waltzUrls: WALTZ_URLS,
        };
        client = new RestClient(config);
      });

      it('should reply with the server content', async () => {
        const res = await client.getStatus();
        expect(res).not.to.be.null;
        res.should.deep.equal('foo');
      });
    });
  });

  describe('getTraceCoinsForward', () => {
    const WALTZ_PATH = '/explorer/trace_coins_forward';
    let client = null;

    describe('when one server has a newer response', () => {
      before(() => {
        mock1
          .post(WALTZ_PATH)
          .query('envelope=true')
          .send({ baz: 1 })
          .reply(200, 'foo', { 'X-Ell-Blockheight': 200 });
        mock2
          .post(WALTZ_PATH)
          .query('envelope=true')
          .send({ baz: 1 })
          .reply(200, 'bar', { 'X-Ell-Blockheight': 100 });
      });

      before(() => {
        const config = {
          waltzUrls: WALTZ_URLS,
        };
        client = new RestClient(config);
      });

      it('should reply with the server content', async () => {
        const res = await client.getTraceCoinsForward({ baz: 1 });
        expect(res).not.to.be.null;
        res.should.deep.equal('foo');
      });
    });
  });

  describe('getTransactionLookup', () => {
    const WALTZ_PATH = '/lookup/txs';
    let client = null;

    describe('when one server has a newer response', () => {
      before(() => {
        mock1
          .post(WALTZ_PATH)
          .query('envelope=true')
          .reply(200, 'foo', { 'X-Ell-Blockheight': 200 });
        mock2
          .post(WALTZ_PATH)
          .query('envelope=true')
          .reply(200, 'bar', { 'X-Ell-Blockheight': 100 });
      });

      before(() => {
        const config = {
          waltzUrls: WALTZ_URLS,
        };
        client = new RestClient(config);
      });

      it('should reply with the server content', async () => {
        const res = await client.getTransactionLookup('foo');
        expect(res).not.to.be.null;
        res.should.deep.equal('foo');
      });
    });
  });

  describe('getTransactionsBatch', () => {
    const WALTZ_PATH = '/txs/batch';
    let client = null;

    describe('when one server has a newer response', () => {
      before(() => {
        mock1
          .post(WALTZ_PATH)
          .query('envelope=true')
          .send({ baz: 1 })
          .reply(200, 'foo', { 'X-Ell-Blockheight': 200 });
        mock2
          .post(WALTZ_PATH)
          .query('envelope=true')
          .send({ baz: 1 })
          .reply(200, 'bar', { 'X-Ell-Blockheight': 100 });
      });

      before(() => {
        const config = {
          waltzUrls: WALTZ_URLS,
        };
        client = new RestClient(config);
      });

      it('should reply with the server content', async () => {
        const res = await client.getTransactionsBatch({ baz: 1 });
        expect(res).not.to.be.null;
        res.should.deep.equal('foo');
      });
    });
  });

  describe('getTransactionsWithinDateRange', () => {
    const WALTZ_PATH = '/txs/search';
    let client = null;

    describe('when one server has a newer response', () => {
      before(() => {
        mock1
          .get(WALTZ_PATH)
          .query({ envelope: 'true', fizz: 'buzz' })
          .reply(200, 'foo', { 'X-Ell-Blockheight': 200 });
        mock2
          .get(WALTZ_PATH)
          .query({ envelope: 'true', fizz: 'buzz' })
          .reply(200, 'bar', { 'X-Ell-Blockheight': 100 });
      });

      before(() => {
        const config = {
          waltzUrls: WALTZ_URLS,
        };
        client = new RestClient(config);
      });

      it('should reply with the server content', async () => {
        const res = await client.getTransactionsWithinDateRange({ fizz: 'buzz' });
        expect(res).not.to.be.null;
        res.should.deep.equal('foo');
      });
    });
  });

  describe('getTxDetail', () => {
    const WALTZ_PATH = '/tx/foo';
    let client = null;

    describe('when one server has a newer response', () => {
      before(() => {
        mock1
          .get(WALTZ_PATH)
          .query('block_height=10&envelope=true')
          .reply(200, 'foo', { 'X-Ell-Blockheight': 200 });
        mock2
          .get(WALTZ_PATH)
          .query('block_height=10&envelope=true')
          .reply(200, 'bar', { 'X-Ell-Blockheight': 100 });
      });

      before(() => {
        const config = {
          waltzUrls: WALTZ_URLS,
        };
        client = new RestClient(config);
      });

      it('should reply with the server content', async () => {
        const res = await client.getTxDetail(10, 'foo');
        expect(res).not.to.be.null;
        res.should.deep.equal('foo');
      });
    });
  });

  describe('getTxExposureIntent', () => {
    const WALTZ_PATH = '/tx/foo/flatExposureSource';
    let client = null;

    describe('when one server has a newer response', () => {
      before(() => {
        mock1
          .get(WALTZ_PATH)
          .query('envelope=true')
          .reply(200, 'foo', { 'X-Ell-Blockheight': 200 });
        mock2
          .get(WALTZ_PATH)
          .query('envelope=true')
          .reply(200, 'bar', { 'X-Ell-Blockheight': 100 });
      });

      before(() => {
        const config = {
          waltzUrls: WALTZ_URLS,
        };
        client = new RestClient(config);
      });

      it('should reply with the server content', async () => {
        const res = await client.getTxExposureIntent('foo');
        expect(res).not.to.be.null;
        res.should.deep.equal('foo');
      });
    });
  });

  describe('getTxhashTransactions', () => {
    const WALTZ_PATH = '/tx-lookup/foo';
    let client = null;

    describe('when one server has a newer response', () => {
      before(() => {
        mock1
          .get(WALTZ_PATH)
          .query('envelope=true')
          .reply(200, 'foo', { 'X-Ell-Blockheight': 200 });
        mock2
          .get(WALTZ_PATH)
          .query('envelope=true')
          .reply(200, 'bar', { 'X-Ell-Blockheight': 100 });
      });

      before(() => {
        const config = {
          waltzUrls: WALTZ_URLS,
        };
        client = new RestClient(config);
      });

      it('should reply with the server content', async () => {
        const res = await client.getTxhashTransactions('foo');
        expect(res).not.to.be.null;
        res.should.deep.equal('foo');
      });
    });
  });

  describe('createTxTrace', () => {
    const WALTZ_PATH = '/tx/foo/trace';
    let client = null;

    describe('when one server has a newer response', () => {
      before(() => {
        mock1
          .post(WALTZ_PATH)
          .query('envelope=true')
          .reply(200, 'foo', { 'X-Ell-Blockheight': 200 });
        mock2
          .post(WALTZ_PATH)
          .query('envelope=true')
          .reply(200, 'bar', { 'X-Ell-Blockheight': 100 });
      });

      before(() => {
        const config = {
          waltzUrls: WALTZ_URLS,
        };
        client = new RestClient(config);
      });

      it('should reply with the server content', async () => {
        const res = await client.createTxTrace('foo');
        expect(res).not.to.be.null;
        res.should.deep.equal('foo');
      });
    });
  });

  describe('pingRpc', () => {
    const WALTZ_PATH = '/ping';
    let client = null;

    describe('when one server has a newer response', () => {
      before(() => {
        mock1
          .get(WALTZ_PATH)
          .query('')
          .reply(200, 'foo', { 'X-Ell-Blockheight': 200 });
        mock2
          .get(WALTZ_PATH)
          .query('')
          .reply(200, 'bar', { 'X-Ell-Blockheight': 100 });
      });

      before(() => {
        const config = {
          waltzUrls: WALTZ_URLS,
        };
        client = new RestClient(config);
      });

      it('should reply with the server content', async () => {
        const res = await client.pingRpc();
        expect(res).not.to.be.null;
        res.should.deep.equal('foo');
      });
    });
  });
});
