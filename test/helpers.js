const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const chaiUuid = require('chai-uuid');

global.chai = chai;
global.expect = chai.expect;
global.should = chai.should();

chai.use(chaiAsPromised);
chai.use(chaiUuid);
