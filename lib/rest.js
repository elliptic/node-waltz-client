const request = require('request-promise');
const Promise = require('bluebird');
const _ = require('lodash');
const E = require('http-errors');
const uuidv4 = require('uuid/v4');
const querystring = require('querystring');

const Messages = {
  WALTZ_NOT_AVAILABLE: 'No Waltz servers are available.',
  UNEXPECTED_ERROR: 'Something unexpected happened',
  READ_TIMEOUT: 'Timed out before any response from Waltz was received',
};

// Pick by status
const _isSuccessfulResponse = x => x.statusCode >= 200 && x.statusCode < 300;
const _is4xxResponse = x => x.statusCode >= 400 && x.statusCode < 500;
const _is5xxResponse = x => x.statusCode >= 500;

module.exports = class RestClient {
  constructor(config, contexty) {
    this.contexty = contexty || {};

    const { waltzUrls, ssl, timeoutMs } = config;

    const sslOptions = _.defaultTo(ssl, {});

    const {
      key, cert, ca, disableHostnameChecking,
    } = sslOptions;

    if (!(_.isArray(waltzUrls) && waltzUrls.length >= 1)) {
      throw new Error('At least one Waltz URL needs to be specified');
    }

    this.waltzUrls = waltzUrls;
    this.clientOptions = {
      json: true,
      resolveWithFullResponse: true,
      key,
      cert,
      ca,
      ...(timeoutMs ? { timeout: timeoutMs } : {}),
    };

    if (disableHostnameChecking) {
      // disable hostname checking
      this.clientOptions.checkServerIdentity = () => undefined;
    }

    this.call = this.call.bind(this);
    this._pickResponse = this._pickResponse.bind(this);

    this.createTxTrace = this.createTxTrace.bind(this);
    this.getAddress = this.getAddress.bind(this);
    this.getAddressAutocomplete = this.getAddressAutocomplete.bind(this);
    this.getAddressCluster = this.getAddressCluster.bind(this);
    this.getAddresses = this.getAddresses.bind(this);
    this.getAddressesTransactions = this.getAddressesTransactions.bind(this);
    this.getAddressLookup = this.getAddressLookup.bind(this);
    this.getAddressTransactions = this.getAddressTransactions.bind(this);
    this.getClusterAddresses = this.getClusterAddresses.bind(this);
    this.getClusterEdge = this.getClusterEdge.bind(this);
    this.getClusterEdgeTransactions = this.getClusterEdgeTransactions.bind(this);
    this.getClusterExplorerData = this.getClusterExplorerData.bind(this);
    this.getClusterExplorerEdges = this.getClusterExplorerEdges.bind(this);
    this.getClusterExplorerNeighbours = this.getClusterExplorerNeighbours.bind(this);
    this.getClusterExposure = this.getClusterExposure.bind(this);
    this.getClusterPiiBackward = this.getClusterPiiBackward.bind(this);
    this.getClusterPiiForward = this.getClusterPiiForward.bind(this);
    this.getClusterRiskyBackward = this.getClusterRiskyBackward.bind(this);
    this.getClusterRiskyForward = this.getClusterRiskyForward.bind(this);
    this.getClusters = this.getClusters.bind(this);
    this.getClusterTransactions = this.getClusterTransactions.bind(this);
    this.getEdgesBatch = this.getEdgesBatch.bind(this);
    this.getLabelAutocomplete = this.getLabelAutocomplete.bind(this);
    this.getStatus = this.getStatus.bind(this);
    this.getTraceCoinsForward = this.getTraceCoinsForward.bind(this);
    this.getTransactionLookup = this.getTransactionLookup.bind(this);
    this.getTransactionsWithinDateRange = this.getTransactionsWithinDateRange.bind(this);
    this.getTransactionsBatch = this.getTransactionsBatch.bind(this);
    this.getTxDetail = this.getTxDetail.bind(this);
    this.getTxExposureIntent = this.getTxExposureIntent.bind(this);
    this.getTxhashTransactions = this.getTxhashTransactions.bind(this);
    this.pingRpc = this.pingRpc.bind(this);

    // functions supported by the celery queue only
    // this.getAddressClusterRpc = this.getAddressClusterRpc.bind(this);
    // this.getSourceOfFundsRpc = this.getSourceOfFundsRpc.bind(this);
    // this.submitAddressCluster = this.submitAddressCluster.bind(this);
    // this.submitTxQueries = this.submitTxQueries.bind(this);
  }

  _getRequestId() {
    if (!this.contexty.context || !this.contexty.context.requestId) {
      return uuidv4();
    }
    return this.contexty.context.requestId;
  }

  call(method, resource, body) {
    const opts = {
      method,
      body,
      headers: {
        'X-Request-Id': this._getRequestId(),
      },
    };

    return Promise.all(_.map(this.waltzUrls, (url) => {
      const options = _.merge({ uri: `${url}${resource}` }, opts, this.clientOptions);
      return request(options).catch(e => e);
    }))
      .reduce(this._pickResponse)
      .then(this._formatResponse);
  }

  _formatResponse(res) {
    const { statusCode, body, error = {} } = res;
    if (statusCode === 200) {
      return body;
    }
    if (_.isNil(statusCode) || _is5xxResponse(res)) {
      if (error.code === 'ESOCKETTIMEDOUT') {
        throw new E(503, Messages.READ_TIMEOUT, { code: 10101 });
      }
      throw new E.InternalServerError(Messages.WALTZ_NOT_AVAILABLE);
    }

    const { message, code, description } = error;
    throw new E(statusCode, message, { code, description });
  }

  _pickResponse(r1, r2) {
    const pickHigher = (a, b) => {
      const height1 = this._getBlockHeight(a);
      const height2 = this._getBlockHeight(b);
      return height1 > height2 ? a : b;
    };

    const r1Success = _isSuccessfulResponse(r1);
    const r2Success = _isSuccessfulResponse(r2);

    if (r1Success && r2Success) {
      return pickHigher(r1, r2);
    } else if (!r1Success && !r2Success) {
      // both have errors
      if (_is4xxResponse(r1) && _is4xxResponse(r2)) {
        return pickHigher(r1, r2);
      } else if (_is4xxResponse(r1) && _is5xxResponse(r2)) {
        return r1;
      } else if (_is4xxResponse(r2) && _is5xxResponse(r1)) {
        return r2;
      }
      throw new E.InternalServerError(Messages.WALTZ_NOT_AVAILABLE);
    }
    return r1Success ? r1 : r2;
  }

  _getBlockHeight(response) {
    return _.get(response, 'headers.x-ell-blockheight', -1);
  }

  createTxTrace(hash, body) {
    return this.call('POST', `/tx/${hash}/trace?envelope=true`, body);
  }

  getAddress(id) {
    return this.call('GET', `/address/${id}?envelope=true`);
  }

  getAddressAutocomplete(base58) {
    return this.call('GET', `/autocomplete/addresses/${base58}?envelope=true`);
  }

  getAddressCluster(id) {
    return this.call('GET', `/address/${id}/cluster?envelope=true`);
  }

  getAddresses(body) {
    return this.call('POST', '/addresses?envelope=true', body);
  }

  getAddressesTransactions(body) {
    return this.call('POST', '/txs?envelope=true', body);
  }

  getAddressLookup(address) {
    return this.call('POST', '/lookup/addresses?envelope=true', { address });
  }

  getAddressTransactions(base58, params) {
    return this.call('POST', `/address/${base58}/txs?envelope=true`, params);
  }

  getClusterAddresses(base58, params) {
    return this.call('POST', `/cluster/${base58}/addresses?envelope=true`, params);
  }

  getClusterEdge(source, target) {
    return this.call('GET', `/cluster-edge/${source}/${target}?envelope=true`);
  }

  getClusterEdgeTransactions(source, target, body) {
    return this.call('POST', `/cluster-edge/${source}/${target}/txs?envelope=true`, body);
  }

  getClusterExplorerData(body) {
    return this.call('POST', '/explorer/basic?envelope=true', body);
  }

  getClusterExplorerEdges(body) {
    return this.call('POST', '/explorer/edges?envelope=true', body);
  }

  getClusterExplorerNeighbours(base58, body) {
    return this.call('POST', `/explorer/clusters/${base58}/neighbours?envelope=true`, body);
  }

  getClusterExposure(base58) {
    return this.call('GET', `/address/${base58}/exposureSource?envelope=true`);
  }

  getClusterPiiBackward(body) {
    return this.call('POST', '/explorer/pii_backward?envelope=true', body);
  }

  getClusterPiiForward(body) {
    return this.call('POST', '/explorer/pii_forward?envelope=true', body);
  }

  getClusterRiskyBackward(body) {
    return this.call('POST', '/explorer/risky_backward?envelope=true', body);
  }

  getClusterRiskyForward(body) {
    return this.call('POST', '/explorer/risky_forward?envelope=true', body);
  }

  getClusters(body) {
    return this.call('POST', '/clusters?envelope=true', body);
  }

  getClusterTransactions(base58, body) {
    return this.call('POST', `/cluster/${base58}/txs?envelope=true`, body);
  }

  getEdgesBatch(body) {
    return this.call('POST', '/cluster-edges/batch?envelope=true', body);
  }

  getLabelAutocomplete(name) {
    return this.call('GET', `/autocomplete/labels/${name}?envelope=true`);
  }

  getStatus() {
    return this.call('GET', '/status');
  }

  getTraceCoinsForward(body) {
    return this.call('POST', '/explorer/trace_coins_forward?envelope=true', body);
  }

  getTransactionLookup(transaction) {
    return this.call('POST', '/lookup/txs?envelope=true', { transaction });
  }

  getTransactionsBatch(body) {
    return this.call('POST', '/txs/batch?envelope=true', body);
  }

  getTransactionsWithinDateRange(params) {
    const queryParams = querystring.stringify(params);
    return this.call('GET', `/txs/search?envelope=true&${queryParams}`);
  }

  getTxDetail(blockHeight, hash) {
    return this.call('GET', `/tx/${hash}?block_height=${blockHeight}&envelope=true`);
  }

  getTxExposureIntent(hash) {
    return this.call('GET', `/tx/${hash}/flatExposureSource?envelope=true`);
  }

  getTxhashTransactions(hash) {
    return this.call('GET', `/tx-lookup/${hash}?envelope=true`);
  }

  pingRpc() {
    return this.call('GET', '/ping');
  }
};
