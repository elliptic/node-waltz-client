const rabbus = require('rabbus');
const autoBind = require('auto-bind');

class RabbotReceiver extends rabbus.Receiver {
  constructor(rabbit, messageHandler, limit) {
    super(rabbit, {
      exchange: { name: 'waltz', type: 'topic' },
      queue: {
        name: 'analyses.request',
        limit,
        noBatch: true,
      },
      routingKey: 'customerTx.txTrace.request',
    });

    this.receive(messageHandler);
  }
}

class RabbotSender extends rabbus.Sender {
  constructor(rabbit) {
    super(rabbit, {
      exchange: { name: 'waltz', type: 'topic' },
      routingKey: 'customerTx.txTrace.response',
    });
  }
}

module.exports = class RabbotClient {
  constructor(rabbit, restClient, config = {}) {
    this.restClient = restClient;
    autoBind(this);
    this.sender = new RabbotSender(rabbit);
    this.receiver = new RabbotReceiver(rabbit, this.txTrace, config.limit);
  }
  async txTrace(message, properties, actions, next) {
    const { tx_hash, body } = message;
    const result = await this.restClient.createTxTrace(tx_hash, body).catch(e => e);
    actions.ack();
    this.respond({ id: body.external_id, result });
  }

  respond(message, callback = () => {}) {
    this.sender.send(message, callback);
  }

  async stop() {
    await this.sender.stop();
    return this.receiver.stop();
  }
};
